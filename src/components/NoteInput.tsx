import React, {ChangeEvent, useState} from "react";

interface NoteInputProps {
    addNote(note: string): void;
}

export const NoteInput: React.FC<NoteInputProps> = ({addNote}) => {

    const [note, setNote] = useState('')

    const updateNote = (event: ChangeEvent<HTMLInputElement>) => {
        setNote(event.target.value)
    }

    const onAddNoteClick = (): void => {
        if (note.length > 0) {
            addNote(note)
        }
        setNote('')
    }

    return (
        <div>
            <input onChange={updateNote} value={note} type='text' name='note' placeholder='Note'/>
            <button onClick={onAddNoteClick}>Add note</button>
        </div>
    )
}
