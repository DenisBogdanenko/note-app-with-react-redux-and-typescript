import React from 'react';
import './App.css';
import {NoteInput} from "./components/NoteInput";
import {useDispatch, useSelector} from "react-redux";
import {NotesState} from "./store/notesReducer";
import {addNoteAction} from "./store/actions";

function App() {
    const notes = useSelector<NotesState, NotesState['notes']>(state => state.notes)
    const dispatch = useDispatch();

    const addNote = (note: string) => {
        dispatch(addNoteAction(note))
    }

    return (
        <div className="App">
            <NoteInput addNote={addNote}/>
            <hr/>
            <ul>
                {notes.map((note) => {
                    return (<li key={note}>{note}</li>)
                })}
            </ul>
        </div>
    );
}

export default App;
